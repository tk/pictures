/*水印*/
create table if not exists `watermark`(
wmid int not null AUTO_INCREMENT,
sitekey varchar(64) not null default '' comment '站点标示' ,
location varchar(64) not null default '' comment '水印位置',
watermarkpath varchar(1024) not null default '' comment '对应站点的水印',
opacity varchar(10) not null default '0.8' comment '透明度',
PRIMARY KEY (wmid)
)ENGINE=MyISAM;

/*原图片存放*/
create table if not exists `picture_path`(
ppid int not null AUTO_INCREMENT,
sitekey varchar(64) not null default '' comment '站点标示',
sitename varchar(128) not null default '' comment '站点名称',
domain varchar(255) not null default '' comment '对应该路径的外网域名 eg:http://img.emisky.com',
abspath varchar(1024) not null default '' comment '保存的绝对路径，如果以http://开头 则发送byte[]',
originalpath varchar(1024) not null default '' comment '原始图片,上传后未作处理的图片',
PRIMARY KEY (ppid)
)ENGINE=MyISAM;

/*图片大小信息*/
create table if not exists `picture_size`(
psid int not null AUTO_INCREMENT,
name varchar(24) not null default '' comment '图片大小标示', 
sitekey varchar(64) not null default '' comment '站点标示' ,
width int not null default 0 comment '图片宽度',
height int not null default 0 comment '图片高度',
PRIMARY KEY (psid)
)ENGINE=MyISAM;

/*图片信息*/
create table if not exists `picture`(
pid bigint not null AUTO_INCREMENT,
guid varchar(32) not null default '' comment '每一个图片有一个唯一的guid',
sitekey varchar(64) not null default '' comment '站点标示',
suffix varchar(8) not null default '' comment '文件后缀',
width int not null default 0 comment '图片宽度',
height int not null default 0 comment '图片高度',
size int not null default 0 comment '图片大小 byte',
userid bigint not null default 0 comment '对应用户id',
username varchar(255) not  null default '' comment '用户名称',
clientip varchar(24) not null default '0.0.0.0' comment '上传者ip',
createtime bigint not null default 0 comment '创建时间',
status int not null default 0 comment '图片状态,0:正常 -1:删除',
md5 varchar(32) not null default '' comment '每一个图片有为一的md5 ,保证每一个站点只图片只上传一次',
isthum int not null default 1 comment '是否接受缩略图  0:不生成  1:可生成缩略图',
iscrop int not null default 1 comment '是否接受裁剪 0:不裁剪 1:裁剪',
iswatermark int not null default 1 comment '是否加水印 0:不加水印 1:添加水印',
isautowidth int not null default 1 comment '是否自动转成宽800 0：不转宽800,1：转800',
is_create_other_size_picture int not null default 0 comment '是否自动生成其它尺寸文件 0：不自动,1：生成',
PRIMARY KEY (pid)
)ENGINE=MyISAM;

alter table picture add index index_picture_guid (`guid`) ;
alter table picture add unique (`guid`) ;

alter table picture add index index_picture_md5 (`md5`) ;
alter table picture add unique (`md5`) ;


/*ALTER TABLE `picture` ADD PRIMARY KEY ( `guid` ) 
ALTER TABLE `picture` ADD UNIQUE (`guid`)*/ 