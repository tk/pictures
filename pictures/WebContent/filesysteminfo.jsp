<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>获取绝对路径</title>
</head>
<body>
当前WEB应用的物理路径：<%=application.getRealPath("/")%><BR>
当前你求请的JSP文件的物理路径：<%=application.getRealPath(request.getRequestURI())%><BR>

</body>
</html>