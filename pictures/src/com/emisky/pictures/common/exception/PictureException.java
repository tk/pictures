package com.emisky.pictures.common.exception;

public class PictureException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	
    public PictureException() {
	super();
    }

  
    public PictureException(String message) {
	super(message);
    }

   
    public PictureException(String message, Throwable cause) {
        super(message, cause);
    }

    public PictureException(Throwable cause) {
        super(cause);
    }
}
