package com.emisky.pictures.entity;

import com.emisky.pictures.common.CommonUtils;

/**
 * 图片路径
 * @author Administrator
 *
 */
public class PicturePathEntity {
	private int ppid;
	private String siteKey;
	private String siteName;
	private String domain;
	private String abspath;
	private String originalpath;
	public int getPpid() {
		return ppid;
	}
	public void setPpid(int ppid) {
		this.ppid = ppid;
	}
	public String getSiteKey() {
		return siteKey;
	}
	public void setSiteKey(String siteKey) {
		this.siteKey = siteKey;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getAbspath() {
		return abspath;
	}
	public void setAbspath(String abspath) {
		this.abspath =CommonUtils.parsePath(abspath);
	}
	
	@Override
	public String toString() {
		String value=this.getPpid()+this.getSiteKey()+this.getSiteName();
		return value;
	}
	public String getOriginalpath() {
		return originalpath;
	}
	public void setOriginalpath(String originalpath) {
		this.originalpath =CommonUtils.parsePath(originalpath) ;
	}
	
}
