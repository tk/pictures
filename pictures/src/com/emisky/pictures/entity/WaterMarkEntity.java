package com.emisky.pictures.entity;
/**
 * 站点水印信息
 * @author Administrator
 *
 */
public class WaterMarkEntity {
	private int wmid;
	private String sitekey;
	private String location;
	private String watermarkpath;
	private String opacity;
	public int getWmid() {
		return wmid;
	}
	public void setWmid(int wmid) {
		this.wmid = wmid;
	}
	public String getSitekey() {
		return sitekey;
	}
	public void setSitekey(String sitekey) {
		this.sitekey = sitekey;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getWatermarkpath() {
		return watermarkpath;
	}
	public void setWatermarkpath(String watermarkpath) {
		this.watermarkpath = watermarkpath;
	}
	public String getOpacity() {
		return opacity;
	}
	public void setOpacity(String opacity) {
		this.opacity = opacity;
	}
	
}
