package com.emisky.pictures.entity;

import java.util.List;

import com.emisky.pictures.common.CommonUtils;
import com.emisky.pictures.common.PictureHelper;

/**
 * 图片信息
 * @author Administrator
 *
 */
public class PictureEntity {
	public PictureEntity(){
		this.guid=CommonUtils.buildGuid();
	}
	public PictureEntity(String siteKey,
			String suffix,
			int width,
			int height,
			int size,long userId,String userName,String clientIp){
		this.siteKey=siteKey;
		this.suffix=suffix;
		this.width=width;
		this.height=height;
		this.size=size;
		this.userId=userId;
		this.userName=userName;
		this.clientIp=clientIp;
		this.iscrop=1;
		this.iswatermark=1;
		this.isautowidth=1;
		this.isthum=1;
	}
	/**
	 * 自增id
	 */
	private long pid;
	/**
	 * guid
	 */
	private String guid;
	/**
	 * 站点id
	 */
	private String siteKey;
	/**
	 * 后缀
	 */
	private String suffix;
	/**
	 * 图片宽度
	 */
	private int width;
	/**
	 * 图片高度
	 */
	private int height;
	/**
	 * 图片大小b
	 */
	private int size;
	/**
	 * 用户id
	 */
	private long userId;
	/**
	 * 用户名称
	 */
	private String userName;
	/**
	 * 用户ip
	 */
	private String clientIp;
	/**
	 * 状态 0默认 -1 删除
	 */
	private int status;
	/**
	 * 文件的二进制数据
	 */
	private byte[] binaryData;
	/**
	 * 图片文件的md5
	 */
	private String md5;
	
	private int iswatermark;
	
	private int iscrop;
	
	private int isautowidth;
	
	private int isthum;
	
	public long getPid() {		
		return pid;
	}
	public void setPid(long pid) {
		this.pid = pid;
	}
	public String getGuid() {
		if(this.guid==null || this.guid==""){
			this.guid=CommonUtils.buildGuid();
		}
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getSiteKey() {
		return siteKey;
	}
	public void setSiteKey(String siteKey) {
		this.siteKey = siteKey;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public byte[] getBinaryData() {
		return binaryData;
	}
	public void setBinaryData(byte[] binaryData) {
		this.binaryData = binaryData;
	}
	public String getMd5() {
		return md5;
	}
	public void setMd5(String md5) {
		this.md5 = md5;
	}
	public int getIswatermark() {
		return iswatermark;
	}
	public void setIswatermark(int iswatermark) {
		this.iswatermark = iswatermark;
	}
	public int getIscrop() {
		return iscrop;
	}
	public void setIscrop(int iscrop) {
		this.iscrop = iscrop;
	}
	public int getIsautowidth() {
		return isautowidth;
	}
	public void setIsautowidth(int isautowidth) {
		this.isautowidth = isautowidth;
	}
	public int getIsthum() {
		return isthum;
	}
	public void setIsthum(int isthum) {
		this.isthum = isthum;
	}
	

}
