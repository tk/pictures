package com.emisky.pictures.entity;

import java.util.ArrayList;
import java.util.List;

public class JsonResultEntity {
	public JsonResultEntity(){
		this.result=0;
		this.message="未处理";
		this.data=new ArrayList<String[]>();
	}
	private int result;
	private String message;
	private List<String[]> data;
	public List<String[]> getData() {
		return data;
	}
	public void setData(List<String[]> data) {
		this.data = data;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
